# Integration

This projects requires an up-to-date version of **Terraform** and **Docker** installed locally.

- https://learn.hashicorp.com/tutorials/terraform/install-cli

## Quick execution: Docker + Multipass

This method will allow you to run whole integration project *within* a ubuntu+docker instance. This will download latest `integration` release and run it.

In order to do that execute `sh install_and_run.sh`. This will:

* install brew if not installed
* install multipass if not installed
* launch a multipass vm called `integration`
* install `docker` and `terraform` in instance
* download and running integration project
* exposing the following ports:

-----------------------------------------------------------------------------------------------
| service        | ports                                                                      |
|----------------|----------------------------------------------------------------------------|
| authenticator  | http://integration.local:8100/admin                                        |
| elasticsearch  | integration.local:9200                                                     |
| keycloak       | http://integration.local:8888/auth                                         |
| keymaker       | http://integration.local:8081/v3/api-docs                                  |
| localstack     | http://integration.local:4566, http://integration.local:4571               |
| manifold       | http://integration.local:8080                                              |
| mongo          | integration.local:27017                                                    |
| mycrossref     | http://integration.local:4566/fe-bucket/index.html                         |
| postgres       | integration.local:5432                                                     |
| prometheus     | integration.local:4590                                                     |
| rest-api       | http://integration.local:3000 (api), integration.local:7880 (nrepl)        |
-----------------------------------------------------------------------------------------------

### Stopping integration

As it might be taking quite a lot of resources of your machine, it is recommended to stop it after finishing using it.

```
$ ./stop_integation.sh
```

This command will stop and delete the `integration` instance

## Using multipass+docker for developers

Install Multipass using either [Ubuntu's own installer](https://multipass.run/docs/installing-on-macos) or [via Homebrew](https://formulae.brew.sh/cask/multipass#default).

Then create the docker VM, that is supposed to be a long lived image (don't delete it)
```
$ multipass launch -c 2 -m 8G -d 80G docker
```

If you want to be able to access your docker instance from OSX using `docker.local` hostname you must install `avahi-daemon` inside your `docker` instance
```
$ multipass exec docker -- sudo apt-get install -y avahi-daemon
```

Now we want to expose docker on port `docker.local:2375`.
```
$ multipass exec docker -- sudo sed -i -r 's/(ExecStart.*containerd.sock)/\1 -H tcp:\/\/0.0.0.0:2375/g' /etc/systemd/system/multi-user.target.wants/docker.service /usr/lib/systemd/system/docker.service
$ multipass exec docker -- sudo systemctl daemon-reload
$ multipass exec docker -- sudo systemctl restart docker.service
$ multipass exec docker -- sudo bash
# cat <<EOF > /etc/rc.local
#!/bin/bash
docker ps
EOF
# chmod 555 /etc/rc.local
# echo "vm.max_map_count=262144" >> /etc/sysctl.conf
```

So, in order to be able to use docker form your OSX host operating system, you need to install docker (only client) and set the environment variable DOCKER_HOST=docker.local in your .bashrc or .zshrc or any other shell you are using. An additional env var called DOCKERENV needs to be set as well so that the `integration.sh` script can differentiate between CI piipeline, internal multipass or external multipass.
```
$ brew install docker
$ echo export DOCKER_HOST=docker.local:2375 >> ~/.bashrc
$ export DOCKERENV="multipassdev"

# restart shell or open a new one and docker commands should work

$ docker ps -a 
```

From now on, your docker images/volumes/data/containers/networks will live inside the multipass image, and using `docker` command from OSX we only access them "remotely".

Now if you have the `integration` project cloned in your OSX, you can just `cd` into it and execute `./integration up` and it should work by running all the container within the multipass `docker` instance.

If you would like to mount local volumes in docker containers, that means a folder in your host mounted in multipass image, then re-mounted within a docker container (eg: docker run -v /myvolume:/myvolume) DO NOT USE `multipass mount`, as it uses `fuse.sshd` and it is not happy with double-sharing volumes with docker.

The solution requires using NFS

In your OSX:
```
$ sudo vim /etc/exports
# edit /etc/exports and add, make sure the network matches for your multipass network
/path/to/your/dev/dir -mapall=YOURUSERNAME -network 192.168.64.0 -mask=255.255.255.0

sudo nfsd restart

# Check osx firewall and make sure you have opened ports 111 and 2049 on tcp and udp
```

In your docker instance on multipass
```
$ sudo bash
# vim /etc/passwd
 -- replace ubuntu uid from 1000 to 501

# vim /etc/group
 -- add group admin2 with gid 80, and add ubuntu to that group

# find /home -uid 1000 -exec chown -R ubuntu:ubuntu {} ';'
 -- This will replace the uid of existing files

# mkdir -p /path/to/your/dev/dir
# vim /etc/fstab
 -- add the following line
192.168.64.1:/path/to/your/dev/dir	/path/to/your/dev/dir	nfs	user=YOURUSERNAME	0	1

# mount /path/to/your/dev/dir # or just restart
```



## Setting up the environment with Terraform

### Manually

```bash
# need to initialise the first time
terraform init

# we can either plan or apply and they will both produce the plan to be applied
# but we need to apply in order to launch the services
terraform apply

# Best to destroy before any modification to the terraform config
terraform destroy
```

### Integration script

The `integration.sh` script allows us to execute the following actions:

- up       - Launch everything
- down     - Uses terraform destroy to take down everything (does not delete temporary terraform files)
- backend  - Launch only the backend services (localstack, elasticsearch, postgres etc) 
- purge    - Forcefully deletes all terraform temporary files, docker containers and networks

The following actions will launch all needed services for developing the corresponding project minus the project itself

- dev-rest-api
- dev-manifold
- dev-my-crossref
- dev-keymaker

Usage: `./integration.sh up|down|dev-rest-api|dev-manifold|dev-my-crossref|dev-keymaker|backend|purge`

### Terraform environment variables

We can enable or disable specific services using the environment variables stored in `terraform.tfvars`

- enable_rest_api = true 
- enable_manifold = true
- enable_my_crossref = true
- enable_keymaker = true
- enable_keycloak = true

```bash 
# Environment variables can also be set through the cli like so:
./integration.sh backend -var enable_manifold=true -var  enable_rest_api=false
```

> For now all use cases are covered by the integration script but as we add more services
using the environment variables will become handy for disabling more than one service at a time.   

## Testing the REST API

We can use files available in the REST API repository: 

```bash
# From within our copy of the REST API project
awslocal s3 cp dev-resources/feeds/corpus/crossref-unixsd-ed1a42bb-3ff3-485a-af0b-511ee92dd923.body s3://md-bucket/ed1a42bb-3ff3-485a-af0b-511ee92dd923/unixsd.xml
```

A few seconds later we should be able to see data at http://localhost:3000/works

## Accessing the MyCrossref Frontend

On startup, the frontend assets are built and deployed to the `fe-bucket` S3 bucket, available locally at
http://localhost:4566/fe-bucket/index.html

### Frontend-specific environment variables

- my_crossref_auth_api_host = "https://doi.crossref.org"

## Service ports

This is the list of ports used by the services once the terraform starts:

-----------------------------------------------------------------------------------------------
| service        | ports                                                                      |
|----------------|----------------------------------------------------------------------------|
| authenticator  | http://docker.local:8100/admin                                             |
| elasticsearch  | docker.local:9200                                                          |
| keycloak       | http://docker.local:8888/auth                                              |
| keymaker       | http://docker.local:8081/v3/api-docs                                       |
| localstack     | http://docker.local:4566, http://docker.local:4571                         |
| manifold       | http://docker.local:8080                                                   |
| mongo          | docker.local:27017                                                         |
| mycrossref     | http://docker.local:4566/fe-bucket/index.html                              |
| postgres       | docker.local:5432                                                          |
| prometheus     | docker.local:4590                                                          |
| rest-api       | http://docker.local:3000 (api), docker.local:7880 (nrepl)                  |
-----------------------------------------------------------------------------------------------

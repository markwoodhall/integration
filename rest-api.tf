variable "enable_rest_api" {
  type    = bool
  default = false
}

variable "enable_elasticsearch" {
  type    = bool
  default = false
}

variable "enable_mongo" {
  type    = bool
  default = false
}

# REST API
resource "docker_image" "rest_api" {
  count          = var.enable_rest_api ? 1 : 0
  name         = "registry.gitlab.com/crossref/rest_api:main"
  keep_locally = true
}

resource "docker_container" "rest_api" {
  count          = var.enable_rest_api ? 1 : 0
  image          = docker_image.rest_api[0].latest
  name           = "integration-rest-api"
  remove_volumes = true
  command        = [
    "lein", "run", ":nrepl", ":api", ":deposit-api", ":create-mappings", ":update-index-settings", ":sqs-ingest",
    ":update-members", ":update-journals", ":update-funders", ":update-subjects"
  ]
  working_dir    = "/usr/src/app"
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 3000
    external = 3000
    ip       = var.docker_ports_ip
  }
  ports {
    internal = 7880
    external = 7880
    ip       = var.DOCKERIP
  }
  env            = [
    "API_PORT=3000",
    "NREPL_PORT=7880",
    "NREPL_DOCKER=1",
    "ELASTICSEARCH_URL=http://integration-elasticsearch:9200",
    "ELASTICSEARCH_NO_AUTH=1",
    "MONGO_HOST=integration-mongo",
    "AWS_ENDPOINT=http://integration-localstack:4566",
    "SQS_QUEUE_URL=${aws_sqs_queue.mdqueue.url}",
    "METADATA_LOCAL_STORAGE=1",
    "SNAPSHOT_LOCAL_STORAGE=1",
    "TOKEN_LOCAL_STORAGE=1",
    "METADATA_BUCKET=md-bucket/",
    "SNAPSHOT_BUCKET=sn-bucket/",
    "TK_BUCKET=tk-bucket/",
    "AWS_ACCESS_KEY=${var.aws_access_key}",
    "AWS_SECRET_KEY=${var.aws_secret_key}",
    "AWS_REGION=${var.aws_region}",
    "ELASTICSEARCH_REFRESH_INTERVAL=2s",
    "INDEXING_TIMEOUT=1"
  ]
}

## Mongo
resource "docker_image" "mongo" {
  count        = var.enable_mongo ? 1 : 0
  name         = "mongo"
  keep_locally = true
}

resource "docker_container" "mongo" {
  count          = var.enable_mongo ? 1 : 0
  image          = docker_image.mongo[0].latest
  name           = "integration-mongo"
  remove_volumes = true
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 27017
    external = 27017
    ip       = var.docker_ports_ip
  }
}

# Elasticsearch
resource "docker_image" "elasticsearch" {
  count        = var.enable_elasticsearch ? 1 : 0
  name = "registry.gitlab.com/crossref/elasticsearch-multiarch:latest"
  keep_locally = true
}

resource "docker_container" "elasticsearch" {
  count          = var.enable_elasticsearch ? 1 : 0
  image          = docker_image.elasticsearch[0].latest
  name           = "integration-elasticsearch"
  remove_volumes = true
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 9200
    external = 9200
    ip       = var.docker_ports_ip
  }
  env            = [
    "ES_JAVA_OPTS=-Xms512m -Xmx512m"
  ]
}

resource "aws_s3_bucket" "md_bucket" {
  bucket        = "md-bucket"
  acl           = "private"
  force_destroy = true
  depends_on    = [time_sleep.wait_for_localstack]
}

resource "aws_s3_bucket" "tk_bucket" {
  bucket     = "tk-bucket"
  acl        = "private"
  depends_on = [time_sleep.wait_for_localstack]
}

resource "aws_s3_bucket" "sn_bucket" {
  bucket     = "sn-bucket"
  acl        = "private"
  depends_on = [time_sleep.wait_for_localstack]
}

resource "aws_sns_topic" "mdtopic" {
  name       = "mdtopic"
  depends_on = [time_sleep.wait_for_localstack]
}

resource "aws_sqs_queue" "mdqueue" {
  name       = "mdqueue"
  depends_on = [time_sleep.wait_for_localstack]
}

resource "aws_sns_topic_subscription" "mdqueue_sub_mdtopic" {
  topic_arn = aws_sns_topic.mdtopic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.mdqueue.arn
}

resource "aws_s3_bucket_notification" "mdbucket_notification" {
  bucket = aws_s3_bucket.md_bucket.id
  topic {
    topic_arn = aws_sns_topic.mdtopic.arn
    events    = ["s3:ObjectCreated:*"]
  }
}

enable_manifold      = true
enable_rest_api      = true
enable_keymaker      = true
enable_my_crossref   = true
enable_authenticator = true
enable_s3_router     = true

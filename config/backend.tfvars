enable_postgres      = true
enable_prometheus    = true
enable_elasticsearch = true
enable_mongo         = true
enable_keycloak      = true
enable_mariadb       = true

variable "docker_provider_host" {
  description = "Used to configure the docker host for local development or CI"
  type = string
  default = "unix:///var/run/docker.sock"
}

terraform {
  required_providers {
    aws    = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
  required_version = ">= 0.14.5"
}

provider "aws" {
  region                      = var.aws_region
  access_key                  = var.aws_access_key
  secret_key                  = var.aws_secret_key
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true
  s3_force_path_style         = true

  endpoints {
    s3  = var.localstack_address
    sqs = var.localstack_address
    sns = var.localstack_address
  }

  default_tags {
    tags = {
      Environment = "Local"
      Service     = "LocalStack"
    }
  }
}

provider "docker" {
  host = var.docker_provider_host
}

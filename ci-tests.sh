#! /usr/bin/env sh

export AWS_ACCESS_KEY_ID="test"
export AWS_SECRET_ACCESS_KEY="test"
export AWS_DEFAULT_REGION="us-east-1"

# AWS CLI
apk add --no-cache \
        python3 \
        py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install \
        awscli \
    && rm -rf /var/cache/apk/*

aws --version

# Test REST API
echo "Testing REST API..."

# fetch xml from the REST API repo
wget https://gitlab.com/crossref/rest_api/-/raw/main/dev-resources/feeds/corpus/crossref-unixsd-ed1a42bb-3ff3-485a-af0b-511ee92dd923.body -O crossref-unixsd-ed1a42bb-3ff3-485a-af0b-511ee92dd923.body

# upload to s3
aws --endpoint-url=http://docker:4566 s3 cp crossref-unixsd-ed1a42bb-3ff3-485a-af0b-511ee92dd923.body s3://md-bucket/ed1a42bb-3ff3-485a-af0b-511ee92dd923/unixsd.xml

echo "wait for processing..."
sleep 30
echo "checking REST API..."
wget "http://docker:3000/works" -O - | grep -o "10.1002\\\/jnr.23992" && echo "success" || exit 1
echo "Done testing REST API..."

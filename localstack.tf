variable "localstack_address" {
  description = "Localstack URI"
  type = string
  default = "http://localhost:4566"
}

resource "docker_image" "localstack" {
  name         = "localstack/localstack:0.14.2"
  keep_locally = true
}

resource "docker_container" "localstack" {
  image    = docker_image.localstack.latest
  name     = "integration-localstack"
  remove_volumes = true
  must_run = true
  networks_advanced {
    name = docker_network.integration_network.name
	aliases = ["md-bucket.integration-localstack"]
  }
  ports {
    internal = 4566
    external = 4566
    ip       = var.docker_ports_ip
  }
  env      = [
    "HOSTNAME_EXTERNAL=${var.LOCALSTACK_HOSTNAME_EXTERNAL}",
    "SERVICES=s3,sqs,sns",
    "DEFAULT_REGION=${var.aws_region}",
    "HOST_TMP_FOLDER=/tmp/localstack",
    "EXTRA_CORS_ALLOWED_ORIGINS=http://integration.local:8000"
  ]
  volumes {
    container_path = "/tmp/localstack"
    host_path      = "/tmp/localstack"
  }
  volumes {
    container_path = "/var/run/docker.sock"
    host_path      = "/var/run/docker.sock"
  }
  healthcheck {
    test     = ["CMD", "curl", "http://${var.DOCKERIP}:4566/"]
    interval = "5s"
    timeout  = "10s"
    retries  = 10
  }
}

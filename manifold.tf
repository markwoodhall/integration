variable "enable_manifold" {
  description = "If true, create the manifold container"
  type        = bool
  default     = false
}

variable "enable_postgres" {
  type    = bool
  default = false
}

resource "docker_container" "manifold" {
  count          = var.enable_manifold ? 1 : 0
  image          = "registry.gitlab.com/crossref/manifold:latest"
  name           = "integration-manifold"
  remove_volumes = true
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 8080
    external = 8080
    ip       = var.docker_ports_ip
  }
  env            = [
    "DB_URI=jdbc:postgresql://${docker_container.postgres[0].name}:5432/nexus?user=nexus&password=nexus",
  ]
}

# Postgres
resource "docker_container" "postgres" {
  count          = var.enable_postgres ? 1 : 0
  image          = "postgres"
  name           = "integration-postgres"
  remove_volumes = true
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 5432
    external = 5432
    ip       = var.docker_ports_ip
  }
  env            = [
    "POSTGRES_USER=nexus",
    "POSTGRES_DB=nexus",
    "POSTGRES_PASSWORD=nexus"
  ]
}

resource "aws_sqs_queue" "manifold-mainq" {
  name                      = "manifold-mainq"
  delay_seconds             = 1
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 1
  redrive_policy            = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.manifold-dlq.arn
    maxReceiveCount     = 1
  })
  depends_on                = [time_sleep.wait_for_localstack]
}

resource "aws_sns_topic_subscription" "manifold-mainq_sub_mdtopic" {
  topic_arn = aws_sns_topic.mdtopic.arn
  protocol  = "sqs"
  endpoint  = aws_sqs_queue.manifold-mainq.arn
}

resource "aws_sqs_queue" "manifold-dlq" {
  name                      = "manifold-dlq"
  delay_seconds             = 1
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 1
  redrive_policy            = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.manifold-auditq.arn
    maxReceiveCount     = 2
  })
  depends_on                = [time_sleep.wait_for_localstack]
}

resource "aws_sqs_queue" "manifold-auditq" {
  name       = "manifold-auditq"
  depends_on = [time_sleep.wait_for_localstack]
}

resource "aws_s3_bucket" "render_bucket" {
  bucket        = "render-bucket"
  acl           = "private"
  force_destroy = true
  depends_on    = [time_sleep.wait_for_localstack]
}

resource "aws_sqs_queue" "manifold-rendering" {
  name                      = "manifold-renderingq"
  delay_seconds             = 1
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 1
  redrive_policy            = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.manifold-dlq.arn
    maxReceiveCount     = 1
  })
  depends_on                = [time_sleep.wait_for_localstack]
}

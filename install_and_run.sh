#!/bin/bash

BRANCH=main
if [[ $# == 2 ]]
then
  BRANCH=${2}
fi

BREW=`which brew`
MULTIPASS=`which multipass`

install_integration_image () {
    sudo -k
    echo -e "\nPlease enter your password followed by [ENTER] to enable sudo access.\n"
    sudo echo
    echo -e "Thank you.\n"
    
    if [[ ! -x ${BREW} ]]
    then
      NONINTERACTIVE=1 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
      BREW=/opt/homebrew/bin/brew
    else
      echo -e "Homebrew is already installed.\n"
    fi
    
    if [[ ! -x ${MULTIPASS} ]]
    then
      ${BREW} install --cask multipass
      MULTIPASS=/usr/local/bin/multipass
      echo -e "\nMultipass is starting..."
      sleep 5
    else
      echo "Multipass is already installed."
    fi
    
    echo -e "\nDeleting integration instance, if present..."
    ${MULTIPASS} delete integration &>/dev/null
    ${MULTIPASS} purge &>/dev/null
    echo -e "\nCreating integration instance...\n"
    ${MULTIPASS} launch -n integration -m 8G -c 4 docker
    echo -e "\nConfiguring integration instance...\n"
    ${MULTIPASS} exec integration -- sh -c "sudo apt -y install git avahi-daemon && sudo snap install terraform --classic"
    ${MULTIPASS} exec integration -- sh -c "git clone https://gitlab.com/crossref/integration.git"

}


start_integration() {
    if [ -n "`multipass list | grep 'integration.*Deleted'`" ]; then install_integration_image; fi
    if [ -z "`multipass list | grep integration`" ]; then install_integration_image; fi
    if [ -n "`multipass list | grep 'integration.*Stopped'`" ]; then multipass start integration; fi


    ${MULTIPASS} exec integration -- sh -c "cd integration && git checkout main && git pull && git checkout ${BRANCH}"


    echo -e "\nStopping terraform...\n"
    ${MULTIPASS} exec integration -- sh -c "cd integration && export TF_VAR_LOCALSTACK_HOSTNAME_EXTERNAL=integration.local && export TF_VAR_MY_CROSSREF_IMAGE_TAG=${BRANCH} && DOCKERENV=multipass ./integration.sh down"

    echo -e "\nStarting terraform...\n"
    ${MULTIPASS} exec integration -- sh -c "cd integration && export TF_VAR_LOCALSTACK_HOSTNAME_EXTERNAL=integration.local && export TF_VAR_MY_CROSSREF_IMAGE_TAG=${BRANCH} && DOCKERENV=multipass ./integration.sh up"
    echo -e "\nInstallation complete. My Crossref is now available at http://integration.local:8000 (it may need a few minutes to warm up)"
    echo -e "\nThe following services are available on host integration.local"
    echo ""
    echo "-----------------------------------------------------------------------------------------------"
    echo "| service        | ports                                                                      |"
    echo "|----------------|----------------------------------------------------------------------------|"
    echo "| authenticator  | http://integration.local:8100/admin                                        |"
    echo "| elasticsearch  | http://integration.local:9200                                              |"
    echo "| keycloak       | http://integration.local:8888/auth                                         |"
    echo "| keymaker       | http://integration.local:8081/v3/api-docs                                  |"
    echo "| localstack     | http://integration.local:4566, http://integration.local:4571               |"
    echo "| manifold       | http://integration.local:8080                                              |"
    echo "| mongo          | http://integration.local:27017                                             |"
    echo "| mycrossref     | http://integration.local:8000                                              |"
    echo "| postgres       | http://integration.local:5432                                              |"
    echo "| prometheus     | http://integration.local:4590                                              |"
    echo "| rest-api       | http://integration.local:3000 (api), http://integration.local:7880 (nrepl) |"
    echo "-----------------------------------------------------------------------------------------------"
    echo ""
}

stop_integration() {
	echo -e "Stopping integration image...\n"
    echo -e "\nStopping terraform...\n"
    ${MULTIPASS} exec integration -- sh -c "cd integration && export TF_VAR_LOCALSTACK_HOSTNAME_EXTERNAL=integration.local && export TF_VAR_MY_CROSSREF_IMAGE_TAG=${BRANCH} && DOCKERENV=multipass ./integration.sh down"

	multipass stop integration
}


print_help() {
	echo -e "---- Usage ----\n"
	echo "$0 --delete  - deletes integration instance"
	echo "$0 --start   - starts integration project (starts instance if it has been stopped, will install instance if it does not exist)"
	echo "$0 --stop    - shutsdown instance (will free resources)"

}

case $1 in
  --start)
    start_integration
    exit 0
    ;;
  --stop)
    stop_integration
    exit 0
    ;;
  --delete)
    echo "deleting integration instance";
    multipass delete integration;
	multipass purge;
    ;;
  *)
    print_help
    ;;
esac

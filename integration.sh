#!/usr/bin/env sh

COMMAND=$1
shift

if [ "${DOCKERENV}" = "multipass" ]; then
    export TF_VAR_DOCKERIP=localhost
    export DOCKER_HOST=unix:///var/run/docker.sock
    export TF_VAR_localstack_address=http://$TF_VAR_DOCKERIP:4566
    export TF_VAR_docker_provider_host=$DOCKER_HOST
elif [ "${DOCKERENV}" = "multipassdev" ]; then
    export TF_VAR_DOCKERIP=`ping -c1 docker.local  | egrep -o "[0-9]+\.[0-9]+.[0-9]+.[0-9]+" | head -n1`
    export DOCKER_HOST=tcp://$TF_VAR_DOCKERIP:`echo $DOCKER_HOST | egrep -o "[0-9]+"`
    export TF_VAR_localstack_address=http://$TF_VAR_DOCKERIP:4566
    export TF_VAR_docker_provider_host=$DOCKER_HOST
elif [ "${DOCKERENV}" = "gitlab" ]; then
    export TF_VAR_DOCKERIP=docker
    export TF_VAR_localstack_address="http://docker:4566"
    export TF_VAR_docker_provider_host="tcp://docker:2375/"
    export TF_VAR_docker_ports_ip="0.0.0.0"
else
	echo "your DOCKERENV var must be set to (multipass|multipassdev|gitlab)"
	exit 1;
fi


purge () {
  echo "Deleting temporary terraform files..."
  rm -rf .terraform .terraform.lock.hcl terraform.tfstate terraform.tfstate.backup

  echo "Attempting to stop and remove running containers..."
  for container in integration-localstack integration-rest-api integration-elasticsearch integration-mongo integration-postgres integration-manifold integration-prometheus integration-keycloak integration-keymaker integration-keycloak_setup integration-my-crossref integration-mariadb integration-authenticator integration-s3-router;
    do
      echo "Removing $container...";
      docker rm --volumes --force $container;
    done

  echo "Removing integration-network..."
  docker network list | grep integration-network | cut -f 1 -d' ' | xargs -I{} docker network rm {}

  echo "Done"
}

case $COMMAND in

  up)
    terraform init
    terraform apply -parallelism=100 -auto-approve -var-file config/backend.tfvars -var-file config/services.tfvars "$@"
    ;;

  dev-rest-api)
    terraform init
    terraform apply -auto-approve -var-file config/dev-rest-api.tfvars "$@"
    ;;

  dev-manifold)
    terraform init
    terraform apply -auto-approve -var-file config/dev-manifold.tfvars "$@"
    ;;

  none|dev-my-crossref|dev-keymaker)
    terraform init
    terraform apply -auto-approve -var-file config/dev-keymaker.tfvars "$@"
    ;;

  backend)
    terraform init
    terraform apply -auto-approve -var-file config/backend.tfvars "$@"
    ;;

  down)
    terraform destroy -auto-approve
    ;;

  purge)
    purge
    ;;

  *)
    echo -n "unknown command"
    ;;
esac

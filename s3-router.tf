variable "enable_s3_router" {
  description = "If true, create the s3_router container"
  type        = bool
  default     = false
}

resource "docker_image" "s3_router" {
  name         = "nginx:1.17.4"
  keep_locally = true
}

resource "docker_container" "s3_router" {
  count       = var.enable_s3_router ? 1 : 0
  image       = docker_image.s3_router.latest
  name        = "integration-s3-router"
  remove_volumes = true
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 80
    external = 8000
    ip       = var.docker_ports_ip
  }
  volumes {
    container_path = "/etc/nginx/conf.d/"
    host_path = abspath("./etc/s3-router")
  }
  depends_on = [time_sleep.wait_for_localstack]
}
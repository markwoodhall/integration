variable "enable_authenticator" {
  description = "If true, create the authenticator container"
  type        = bool
  default     = false
}

variable "enable_mariadb" {
  type    = bool
  default = false
}

resource "docker_container" "authenticator" {
  count          = var.enable_authenticator ? 1 : 0
  image          = "registry.gitlab.com/crossref/authenticator:latest"
  name           = "integration-authenticator"
  remove_volumes = true
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 8000
    external = 8100
    ip       = var.docker_ports_ip
  }
  env            = [
           "DEBUG=1",
           "LOG_LEVEL=DEBUG",
           "DB_NAME=authenticator",
           "DB_USERNAME=root",
           "DB_PASSWORD=password",
           "DB_HOST=${docker_container.mariadb[0].name}",
           "DB_PORT=3306",
           "AUTH_SVC_USER=test_internal_user",
           "AUTH_SVC_EMAIL=support@crossref.org",
           "AUTH_SVC_PASSWORD=test_internal_password",
           "ROOT_AUTH_PWD=test_internal_password",
           "ALLOWED_HOSTS=authenticator,localhost,integration-authenticator,docker.local,integration.local,docker",
           "PORT=8000",
           "PYTHONDONTWRITEBYTECODE=1",
           "PYTHONUNBUFFERED=1",
           "TEST_ADMIN_API=1",
		   "KEYCLOAK_URL=http://integration-keycloak:8888/auth/",
           "KEYCLOAK_SYNC_CREDENTIALS=pwdPWD1!",
  ]

  depends_on = [
    time_sleep.wait_for_mariadb
  ]
}

# mariadb
resource "docker_container" "mariadb" {
  count          = var.enable_mariadb ? 1 : 0
  image          = "mariadb:10.5.8"
  name           = "integration-mariadb"
  remove_volumes = true
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 3306
    external = 3306
    ip       = var.docker_ports_ip
  }
  env            = [
    "MYSQL_DATABASE=authenticator",
    "MYSQL_ROOT_PASSWORD=password"
  ]
}

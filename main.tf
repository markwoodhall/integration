variable "aws_region" {
  type = string
  default = "us-east-1"
}

variable "DOCKERIP" {
  type = string
}

variable "LOCALSTACK_HOSTNAME_EXTERNAL" {
  type = string
  description = "Hostname used by localstack services when responding to URL requests"
}

variable "aws_access_key" {
  type = string
  default = "test"
}

variable "aws_secret_key" {
  type = string
  default = "test"
}

variable "docker_ports_ip" {
  description = "Used to globally allow opening up ports externally or not"
  type = string
  default = "0.0.0.0"
}

resource "docker_network" "integration_network" {
  name = "integration-network"
}

resource "time_sleep" "wait_for_localstack" {
  depends_on = [docker_container.localstack]

  create_duration = "30s"
}

resource "time_sleep" "wait_for_keycloak" {
  depends_on = [docker_container.keycloak]

  create_duration = "180s"
}

resource "time_sleep" "wait_for_mariadb" {
  depends_on = [docker_container.keycloak]

  create_duration = "30s"
}

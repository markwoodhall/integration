#! /usr/bin/env sh

attempts=0
url=$1
max_attempts=$2
wait_time=$3

echo "waiting for $url"

until [[ "$(wget --spider -S $url 2>&1 | grep "HTTP/" | awk '{print $2}' | tail -n 1)" == "200" ]]
do
  if [[ $attempts -eq $max_attempts ]]; then
    echo "failed to contact $1"
    exit 1
  fi
  echo "retrying..."
  sleep $wait_time
  attempts=$(($attempts+1))
done

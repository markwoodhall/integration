variable "enable_keycloak" {
  description = "If true, create the keycloak container"
  type        = bool
  default     = false
}

resource "docker_image" "keycloak" {
  name         = "registry.gitlab.com/crossref/idm:latest"
  keep_locally = true
}

resource "docker_container" "keycloak" {
  count       = var.enable_keycloak ? 1 : 0
  image       = docker_image.keycloak.latest
  name        = "integration-keycloak"
  command     = ["start-dev","--spi-login-protocol-openid-connect-legacy-logout-redirect-uri=true","--http-enabled","true","--import-realm"]
  remove_volumes = true
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 8888
    external = 8888
    ip       = var.docker_ports_ip
  }
  ports {
    internal = 8787
    external = 8787
    ip       = var.docker_ports_ip
  }
  env         = [
    "KEYCLOAK_USER=admin",
    "KEYCLOAK_PASSWORD=admin",
    "KEYCLOAK_LOGLEVEL=DEBUG",
    "KEYCLOAK_ADMIN=admin",
    "KEYCLOAK_ADMIN_PASSWORD=admin",
    "KEYCLOAK_LONG_LIVED_ROLE_NAME=ROLE_APIKEY",
    "KEYCLOAK_FRONTEND_URL=http://localhost:8888",
    "AUTHENTICATOR_URL=http://integration-authenticator:8000/",
    "AUTHENTICATOR_INTERNAL_USER=test_internal_user",
    "AUTHENTICATOR_INTERNAL_PASSWORD=test_internal_password",
    "APIKEY_CACHE_TTL=1",
    "DEBUG=true",
    "DEBUG_PORT=*:8787",
  ]
}

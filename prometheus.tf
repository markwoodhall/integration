variable "enable_prometheus" {
  description = "If true, create the prometheus container"
  type        = bool
  default = false
}

resource "docker_image" "prometheus" {
  name = "prom/prometheus"
  keep_locally = true
}

resource "docker_container" "prometheus" {
  count      = var.enable_prometheus ? 1 : 0
  image = docker_image.prometheus.latest
  name  = "integration-prometheus"
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 9090
    external = 4590
    ip       = var.DOCKERIP
  }
  volumes {
    container_path = "/etc/prometheus"
    host_path = abspath("./etc/prometheus")
  }
}

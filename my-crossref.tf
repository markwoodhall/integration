variable "enable_my_crossref" {
  description = "If true, create the my_crossref container"
  type        = bool
  default     = false
}

variable "my_crossref_image_tag" {
  description = "MyCrossref image tag"
  type = string
  default = "latest"
}

variable "my_crossref_auth_api_host" {
  description = "Authentication API host"
  type        = string
  default     = "https://doi.crossref.org"
}


variable "my_crossref_keycloak_url" {
  description = "Keycloak Server URL"
  type        = string
  default     = "http://integration.local:8888/auth"
}

resource "docker_image" "my_crossref" {
  name         = "registry.gitlab.com/crossref/console:${var.my_crossref_image_tag}"
  keep_locally = true
}

resource "docker_container" "my_crossref" {
  count       = var.enable_my_crossref ? 1 : 0
  image       = docker_image.my_crossref.latest
  name        = "integration-my-crossref"
  remove_volumes = true
  command     = [
    "sh", "build-and-deploy.sh"
  ]
  working_dir = "/app"
  networks_advanced {
    name = docker_network.integration_network.name
  }
  ports {
    internal = 8080
    external = 8001
    ip       = var.docker_ports_ip
  }
  env         = [
    "VUE_APP_FORGOT_PASSWORD_LINK=https://authenticator.crossref.org/reset-password/",
    "VUE_APP_REDIRECT_ON_ROLE_AUTHORISATION=0",
    "VUE_APP_HTTP_MOCKING=0",
    "VUE_APP_AUTH_API_BASE_URL=${var.my_crossref_auth_api_host}",
    "VUE_APP_KEYCLOAK_URL=${var.my_crossref_keycloak_url}",
    "VUE_APP_ENABLE_XSTATE_INSPECTOR=0",
    "AWS_ENDPOINT=${var.localstack_address}",
    "AWS_ACCESS_KEY_ID=${var.aws_access_key}",
    "AWS_SECRET_ACCESS_KEY=${var.aws_secret_key}",
    "AWS_DEFAULT_REGION=${var.aws_region}",
    "DEPLOY_S3_BUCKET=${aws_s3_bucket.fe_bucket.bucket}"
  ]
}

resource "aws_s3_bucket" "fe_bucket" {
  bucket     = "fe-bucket"
  acl        = "private"
  force_destroy = true
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
  depends_on = [time_sleep.wait_for_localstack]
}
